<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Createshortcut</name>
    <message>
        <source>Shortcut could not be created in</source>
        <translation>Det gick inte att skapa genväg i</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>&lt;b&gt;Warning!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Varning!&lt;/b&gt;</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.&lt;br&gt;The desktop shortcut will be removed.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Genvägen på skrivbordet kommer att tas bort.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.&lt;br&gt;All shortcuts will be removed.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Alla genvägar kommer att tas bort.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <source>Failed to delete your configuration files.&lt;br&gt;Check your file permissions.</source>
        <translation>Det gick inte att ta bort dina konfigurationsfiler.&lt;br&gt;Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation> kan inte tas bort.&lt;br&gt;Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <source>The shortcut could not be removed:</source>
        <translation>Genvägen kunde inte tas bort:</translation>
    </message>
</context>
</TS>
