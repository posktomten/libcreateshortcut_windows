#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          CREATESHORTCUT (Windows)
#//          Copyright (C) 2022 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/libcreateshortcut
#//          programmering1 (at) ceicer (dot) org
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

# 2021-02-19 00:34
QT += core widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#ipac
CONFIG+=staticlib
HEADERS += \
    createshortcut_win.h \
    createshortcut_win_global.h

SOURCES += \
    createshortcut.cpp \
    removeshortcut.cpp \
    deleteallsettings.cpp



#Export LIBS
DEFINES += CREATECHORTCUT_LIBRARY

TEMPLATE = lib

# By default, qmake will make a shared library. Uncomment to make the library
# static.
#CONFIG += staticlib


# By default, TARGET is the same as the directory, so it will make 
# liblibrary.so or liblibrary.a (in linux).  Uncomment to override.


CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG


equals(BUILD,RELEASE) {

    TARGET=createshortcut

}
equals(BUILD,DEBUG) {

    TARGET=createshortcutd
}

equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
# DESTDIR=C:\Users\posktomten\PROGRAMMERING\libcreateshortcut\testprogram\lib5
# DESTDIR=C:\Users\posktomten\PROGRAMMERING\template\lib5
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
#DESTDIR=C:\Users\posktomten\PROGRAMMERING\checkversionsvtplaydl\lib6
}


#UI_DIR = ../code

TRANSLATIONS += i18n/_libcreateshortcut_win_template_xx_XX.ts \
                i18n/_libcreateshortcut_win_sv_SE.ts \
                i18n/_libcreateshortcut_win_it_IT.ts

#RESOURCES += \


message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
message(PWD: $$PWD)
message (--------------------------------------------------)




