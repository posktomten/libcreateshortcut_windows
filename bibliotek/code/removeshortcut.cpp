//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Windows)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "createshortcut_win.h"
#include <QStandardPaths>
#include <QMessageBox>
#include <QFile>

void Createshortcut::removeApplicationShortcut(QString *display_name)
{
    const QString shortcutlocation = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
    const QString thelink = shortcutlocation + QStringLiteral("/") + *display_name + QStringLiteral(".lnk");

    if(!QFile::remove(thelink)) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(*display_name);
        msgBox.setText(tr("The shortcut could not be removed:") + QStringLiteral("<br>\"") + thelink + QStringLiteral("\""));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
    }
}

void Createshortcut::removeDesktopShortcut(QString *executable_name)
{
    const QString shortcutlocation = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    const QString thelink = shortcutlocation + "/" + *executable_name + ".lnk";

    if(!QFile::remove(thelink)) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(*executable_name);
        msgBox.setText(tr("The shortcut could not be removed:") + QStringLiteral("<br>\"") + thelink + QStringLiteral("\""));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
    }
}

void Createshortcut::removeApplicationShortcutSilent(QString *display_name)
{
    const QString shortcutlocation = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
    const QString thelink = shortcutlocation + QStringLiteral("/") + *display_name + QStringLiteral(".lnk");

    if(QFile::exists(thelink)) {
        QFile::remove(thelink);
    }
}

void Createshortcut::removeDesktopShortcutSilent(QString *executable_name)
{
    const QString shortcutlocation = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    const QString thelink = shortcutlocation + QStringLiteral("/") + *executable_name + QStringLiteral(".lnk");

    if(QFile::exists(thelink)) {
        QFile::remove(thelink);
    }
}
