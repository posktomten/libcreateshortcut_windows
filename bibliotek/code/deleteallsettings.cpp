//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Windows)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "createshortcut_win.h"
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>
#include <QPushButton>
#include <QDir>
#include <QFileInfo>
// #include <QDebug>
bool Createshortcut::deleteAllSettings(QString *display_name, QString *executable_name, QString *version, bool download_install)
{
    QMessageBox *msgBox = new QMessageBox;
    msgBox->setText(tr("<b>Warning!</b>"));
    msgBox->setIcon(QMessageBox::Warning);
    msgBox->setWindowTitle(*display_name + " " + *version);
    QPushButton  *rejectButton = new QPushButton(tr("No"), msgBox);
    QPushButton  *acceptButton = new QPushButton(tr("Yes"), msgBox);
    msgBox->addButton(acceptButton, QMessageBox::YesRole);
    msgBox->addButton(rejectButton, QMessageBox::RejectRole);
    msgBox->setDefaultButton(rejectButton);
#ifdef Q_OS_WIN
    QString file(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + QStringLiteral("/") + *display_name + QStringLiteral(".lnk"));
#endif
    QFileInfo fi(file);

    if(download_install) {
        if(fi.exists()) {
            msgBox->setInformativeText(tr("All your saved settings will be deleted.<br>The desktop shortcut will be removed.<br>And the program will exit.<br>Do you want to continue?"));
            ;
        } else {
            msgBox->setInformativeText(tr("All your saved settings will be deleted.<br>And the program will exit.<br>Do you want to continue?"));
        }
    } else {
        msgBox->setInformativeText(tr("All your saved settings will be deleted.<br>All shortcuts will be removed.<br>And the program will exit.<br>Do you want to continue?"));
    }

    msgBox->exec();

    if(msgBox->clickedButton() == rejectButton) {
        return false;
    }

    if(msgBox->clickedButton() == acceptButton) {
        if(download_install) {
            if(fi.exists()) {
                removeDesktopShortcutSilent(executable_name);
            } else {
            }
        } else {
            removeApplicationShortcutSilent(display_name);
        }

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, *display_name,
                           *executable_name);
        QDir dir;
        QString settingspath = dir.filePath(settings.fileName());
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
        QFileInfo fi(settingspath);
        dir = fi.absolutePath();
#else
        dir.setPath(settingspath);
#endif

        if(!dir.removeRecursively()) {
            QMessageBox *msgBox = new QMessageBox;
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->setWindowTitle(*display_name + " " + *version);
            msgBox->setText(tr("Failed to delete your configuration files.<br>"
                               "Check your file permissions."));
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->exec();
        }

        const QString applicationPath = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/" + *executable_name + ".lnk";

        /***/
        if(QFile::exists(applicationPath)) {
            if(!QFile::remove(applicationPath)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(*executable_name);
                msgBox.setText("\"" + applicationPath + "\"" + tr(" can not be removed.<br>Pleas check your file permissions."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
        }

        /**/
        QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" +*executable_name + ".lnk";

        if(QFile::exists(desktopPath)) {
            if(!QFile::remove(desktopPath)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(*executable_name);
                msgBox.setText("\"" + desktopPath + "\"" + tr(" can not be removed.<br>Pleas check your file permissions."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
        }
    }

    return true;
}

bool Createshortcut::deleteSettingsSilent(QString *display_name, QString *executable_name, QString *version)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, *display_name,
                       *executable_name);
    QDir dir;
    QString settingspath = dir.filePath(settings.fileName());
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
    QFileInfo fi(settingspath);
    dir = fi.absolutePath();
#else
    dir.setPath(settingspath);
#endif

    if(!dir.removeRecursively()) {
        QMessageBox *msgBox = new QMessageBox;
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->setWindowTitle(*display_name + " " + *version);
        msgBox->setText(tr("Failed to delete your configuration files.<br>"
                           "Check your file permissions."));
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->exec();
        return false;
    }

    return true;
}
