#include "dialog.h"
#include "ui_dialog.h"
#include "createshortcut.h"
#include "createshortcut_global.h"
Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    connect(ui->pbCreateApplicationsShortcut, &QPushButton::clicked, []() {
        Createshortcut *mCreateshortcut = new Createshortcut;
        QString *display_name = new QString(QStringLiteral("TestProgram"));
        QString *executable_name = new QString(QStringLiteral("testprogram"));
        mCreateshortcut->makeShortcutFile(display_name, executable_name, true, false);
    });
    connect(ui->pbRemoveApplicationsShortcut, &QPushButton::clicked, []() {
        Createshortcut *mCreateshortcut = new Createshortcut;
        QString *executable_name = new QString(QStringLiteral("testprogram"));
        mCreateshortcut->removeApplicationShortcut(executable_name);
    });
    connect(ui->pbCreateDesktopShortcut, &QPushButton::clicked, []() {
        Createshortcut *mCreateshortcut = new Createshortcut;
        QString *display_name = new QString(QStringLiteral("TestProgram"));
        QString *executable_name = new QString(QStringLiteral("testprogram"));
        mCreateshortcut->makeShortcutFile(display_name, executable_name, false, true);
    });
    connect(ui->pbRemoveDesktopShortcut, &QPushButton::clicked, []() {
        Createshortcut *mCreateshortcut = new Createshortcut;
        QString *executable_name = new QString(QStringLiteral("testprogram"));
        mCreateshortcut->removeDesktopShortcut(executable_name);
    });
}




Dialog::~Dialog()
{
    delete ui;
}

