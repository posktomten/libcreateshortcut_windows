QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    dialog.cpp

HEADERS += \
    dialog.h


FORMS += \
    dialog.ui

INCLUDEPATH += "../include"
DEPENDPATH += "../include"

TARGET = testprogram

win32:RC_ICONS += images/icon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug


}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"

CONFIG (release, debug|release): LIBS += -L../lib6/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcreateshortcutd # Debug


}



message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
